#!/usr/bin/python3

#
# bbb.py
# Frank Schiebel frank@linuxmuster.net
# Adapted for munin by ulif <ulif@riseup.net>
# GPL v3
#

import sys
import socket
import hashlib
import requests
import subprocess
from collections import defaultdict
from xml.dom.minidom import parse, parseString

def get_api_checksum():
    """Get API Secret

    cf. https://docs.bigbluebutton.org/dev/api.html
    """
    output = subprocess.check_output(['bbb-conf', '--secret']).decode('utf-8')
    secret = [x.strip() for x in output.split('\n') if 'Secret: ' in x][0].split(" ")[-1]
    qstringwithsecret = "getMeetings" + secret
    return hashlib.sha1(qstringwithsecret.encode('utf-8')).hexdigest()


def get_meeting_data(checksum):
    """Request current traffic data

    from BBB via XML API.
    """
    fqdn = socket.getfqdn()
    full_query_uri = "https://%s/bigbluebutton/api/getMeetings?checksum=%s" % (fqdn, checksum)
    result = requests.get(full_query_uri)
    return result.status_code, result.text


(status, xml) = get_meeting_data(get_api_checksum())


numMeetings  = 0
numAttendees = 0
numWithVideo = 0
numWithVoice = 0
numListeners = 0


if status != 200:
    print("HTTP return code was not 200/OK")
    sys.exit(-1)


meetingdata = parseString(xml)
returncode=meetingdata.getElementsByTagName("returncode")[0]
returncode=returncode.firstChild.wholeText


if returncode != "SUCCESS":
    print("API returncode was not SUCCESS")
    sys.exit(-1)


# get numbers from active meetings
meetings=meetingdata.getElementsByTagName("meeting")
for m in meetings:
    p = m.getElementsByTagName("bbb-origin-server-name")[0]
    numMeetings += 1
    p = m.getElementsByTagName("participantCount")[0]
    numAttendees += int(p.firstChild.wholeText)
    p = m.getElementsByTagName("listenerCount")[0]
    numListeners += int(p.firstChild.wholeText)
    p = m.getElementsByTagName("voiceParticipantCount")[0]
    numWithVoice += int(p.firstChild.wholeText)
    p = m.getElementsByTagName("videoCount")[0]
    numWithVideo += int(p.firstChild.wholeText)


if "autoconf" in sys.argv:
    print("yes")
    sys.exit(0)
elif "config" in sys.argv:
    print("graph_order listeners voice video meetings attendees")
    print("graph_title BigBlueButton conferences")
    print("graph_args --base 1000 -l 0")
    print("graph_category system")
    print("attendees.label attendees")
    print("attendees.type GAUGE")
    print("attendees.info Number of attendees")
    print("meetings.label meetings")
    print("meetings.type GAUGE")
    print("meetings.info Number of active conferences")
    print("video.label video")
    print("video.type GAUGE")
    print("video.info Number of video streams")
    print("video.draw STACK")
    print("voice.label voice")
    print("voice.type GAUGE")
    print("voice.info Number of voice streams")
    print("voice.draw STACK")
    print("listeners.label listeners")
    print("listeners.type GAUGE")
    print("listeners.info Number of listening attendees")
    print("listeners.draw AREA")
    sys.exit(0)
print("meetings.value %s" % numMeetings)
print("attendees.value %s" % numAttendees)
print("video.value %s" % numWithVideo)
print("voice.value %s" % numWithVoice)
print("listeners.value %s" % numListeners)
sys.exit(0)
