#!/bin/bash

# Pull in the helper functions for configuring BigBlueButton
source /etc/bigbluebutton/bbb-conf/apply-lib.sh

# Enable three different Kurento server instances
# cf. https://docs.bigbluebutton.org/2.2/customize.html
enableMultipleKurentos

enableUFWRules
ufw allow 6556

echo "  - Setting camera defaults"
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[0].bitrate 50
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[1].bitrate 100
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[2].bitrate 200
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[3].bitrate 300

yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[0].default true
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[1].default false
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[2].default false
yq w -i $HTML5_CONFIG public.kurento.cameraProfiles.[3].default false

# cf. https://docs.bigbluebutton.org/2.2/troubleshooting.html#freeswitch-using-default-stun-server
echo "  - Set freeswitch stun server"
xmlstarlet edit --inplace --update '//X-PRE-PROCESS[@cmd="set" and starts-with(@data, "external_rtp_ip=")]/@data' --value "external_rtp_ip=stun:{{ turn_server }}" /opt/freeswitch/conf/vars.xml

ansible-playbook \
    --connection=local \
    -i "/root/bbb-ansible/hosts.local" \
    "/root/bbb-ansible/bbb-apply-config.yml"
