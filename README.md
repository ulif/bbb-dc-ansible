# bbb-dc-ansible

Ansible Playbooks zur Installation von BigBlueButton

Basierend auf Skripten des fantastischen Projekts [Digital souveräne Schule](https://lehrerfortbildung-bw.de/st_digital/medienwerkstatt/dossiers/bbb/index.html) in
Baden-Württemberg:

  https://codeberg.org/DigitalSouveraeneSchule/bbb

Wir administrieren mit diesen Skripten unsere eigenen BigBlueButton-Server.

## Installation BBB

* Einen oder mehrere Server mit Ubuntu 18.04 mit IPv4 Adresse
* DNS Einträge für alle Server müssen angelegt sein (z.B. bbb1.meinserver.de, bbb2.andererpc.de)
* Sicherstellen, dass man sich mit SSH-Key auf den Server verbinden kann und dort sudo-Rechte hat
* Anpassen der Einstellungen für die BBB Version und den Turn-Server im Playbook wenn nötig.
* Wenn man mehrere Maschinen ausrollen möchte, kann man ein Inventory File anlegen und mit ``ansible-playbook -i hosts bbb-install.yml --ask-vault-pass`` alle Maschinen auf einmal installieren.
* Wenn man nur eine Maschine installieren möchte kann man das ohne Inventory tun: ``ansible-playbook -i "bbb.q-gym.de," bbb-install.yml --ask-vault-pass``

*Grundsätzliche Unterschiede unseres Setups zum Setup des DSS-Projekts:*

* Wir benutzen keinen SSH-Root-Zugriff auf die Server
* Sicherstellen, dass man sich als gewöhnliche Userin mit SSH-Key mit dem Server verbinden kann und dort `sudo`-Rechte hat
* Wir benutzen nur `greenlight`, kein `Moodle`.
* Zum Monitoring benutzen wir munin auf einem externen Server und munin-node auf den BBBs.
* Um die Hintergrundmusik zu aktivieren, reicht es eine Datei `hold_music.wav`
  entweder in das lokale Verzeichnis oder in das entfernte Verzeichnis
  `/opt/freeswitch` zu legen. Das Musikstück wird dann abgespielt, sobald sich
  genau eine Person in einer Session befindet. Ein ggf. auf dem Server
  befindliches File wird von einem anderen lokalen überschrieben.


## Feinkonfiguration greenlight

Wir beschränken uns bei der greenlight-Konfiguration auf wenige manuelle
Schritte, die nach der Installation im Webinterface erledigt werden können (als
Admin-User.in unter Name -> Organisation -> Seiteneinstellungen)

1) Wir setzen das Logo auf
    https://bigbrotherawards.de/sites/default/files/grid/2015/02/25/Digitalcourage_logo_-_weiss_RGB.svg

2) Wir setzen die "Normal"-Farbe des Themes auf `#808080` (Standardwert: `#467FCF`)

Für die sonstien Werte setzen wir:

- Registrierungsmethode: `Offene Registrierung`
- Betreten des Raums erfordert Authentifizierung: `Deaktiviert`
- Nutzern erlauben, Räume zu teilen: `Aktiviert`
- Voreinstellung der Aufzeichnungssichtbarkeit: `Nicht gelistet`
- Anzahl der Räume pro Nutzer.in: `15+`


## COTURN/TURN testen


### Ports

Auf dem Server sollten die Ports 80, 443, 3478 und 5349 vom Turnserver belegt
sein. Das lässt sich auf dem Server z.B. mit

    $ sudo netstat -tupan | grep turn | uniq

verifizieren. Es sollten IPs mit den oben gelisteten Ports angezeigt werden:

    ...
    udp        0      0 127.0.0.1:80            0.0.0.0:*           3999/turnserver
    udp        0      0 1.2.3.4:80              0.0.0.0:*           3999/turnserver
    udp        0      0 127.0.0.1:443           0.0.0.0:*           3999/turnserver
    udp        0      0 1.2.3.4:443             0.0.0.0:*           3999/turnserver
    udp        0      0 127.0.0.1:3478          0.0.0.0:*           3999/turnserver
    udp        0      0 1.2.3.4:3478            0.0.0.0:*           3999/turnserver
    udp        0      0 127.0.0.1:5349          0.0.0.0:*           3999/turnserver
    udp        0      0 1.2.3.4:5349            0.0.0.0:*           3999/turnserver

### STUN

Einen netten kleinen STUN-Client bietet unter Debian/Ubuntu das Paket `stun-client`.

    $ sudo apt install stun-client  # falls noch nicht installiert...
    $ stun turn.example.com:3478
    STUN client version x.y
    Primary: Independent Mapping, Independent Filter, preserves ports, will hairpin
    Return value is 0x000003

Die `coturn` package (Debian/Ubuntu) bietet einige hilfreiche Clientprogramme,
mit denen STUN-/TURN-Server von der Kommandozeile aus angesprochen werden
können.

Der STUN-Server kann so angesprochen werden:

    $ sudo apt install coturn  # falls noch nicht installiert...
    $ turnutils_stunclient -p 3478 turn.example.com

was dann in etwa soetwas ergeben sollte:

    0: IPv4. UDP reflexive addr: 1.2.3.4:58519

(mit der je eigenen öffentlichen IP-Adresse)


### TURN

Ein anderer Client der `coturn`-Package erlaubt den Test von TURN-Funktionalität:

    $ sudo apt install coturn  # falls noch nicht installiert...
    $ turnutils_uclient -T -W <STATIC-SECRET> turn.example.com

Gelingende TURN-Kommunikation kann (unwahrscheinlicherweise) so aussehen:

    0: Total connect time is 1
    0: 2 connections are completed
    ...
    6: start_mclient: tot_send_bytes ~ 1000, tot_recv_bytes ~ 1000
    6: Total transmit time is 6
    6: Total lost packets 0...
    6: Average round trip delay 25.600000 ms; min=20 ms, max = 54 ms
    6: Average jitter...


### Trickle ICE

Die WebRTC Trickle ICE-Funktionalität kann über

  https://webrtc.github.io/samples/src/content/peerconnection/trickle-ice/

getestet werden. In das Feld "STUN or TURN URI" die Domain des zu testenden
STUN-Servers eintragen: `stun:stun.example.com`. Danach auf `Gather candidates`
klicken und im Ergebnis sollten `srflx`-Einträge erscheinen.

### WebRTC in Firefox/Chrome

Während der Teilnahme an einer BBB-Sitzung lässen sich WebRTC-Probleme über den Aufruf von

  ``about://webrtc`` (Firefox) oder
  ``chrome://webrtc-internals`` resp.
  ``chrome://webrtc-logs`` (Chromium/Chrome)

in einem weiteren Tab analysieren. Im Firefox werden nach Klick auf `Details
einblenden` erfolgreiche, fehlgegangene und im Aufbau befindliche
WebRTC-Sessions angezeigt.


## Under Construction

**Im Moment ist dieses Projekt im Aufbau und die Informationen hier daher sehr
unvollständig. Wenn dieser Hinweis hier verschwunden ist, sollte alles
benutzbar sein.  Vorher bitten wir von nicht total dringenden Rückfragen
abzusehen, um unseren Arbeitsdruck zu mildern.**

ulif + datenzwerg.in
